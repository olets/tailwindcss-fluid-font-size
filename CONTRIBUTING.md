# Contributing

Thanks for your interest. Contributions are welcome!

> Please note that this project is released with a [Contributor Code of Conduct](CODE_OF_CONDUCT.md). By participating in this project you agree to abide by its terms.

Check the [Issues](https://codeberg.org/olets/tailwindcss-fluid-font-size/issues) to see if your topic has been discussed before or if it is being worked on. You may also want to check the roadmap (see above). Discussing in an Issue before opening a Pull Request means future contributors only have to search in one place.

## Actions

### Contributors

If you open a pull request and see that the triggered action is "waiting to run", don't worry: when a maintainer is ready to review the pull request, they will start the runner and the test will run.

### Maintainers

You can run a self-hosted actions runner from your machine.

If you have already generated a local `.runner` config file for this repo, start the runner

```shell
docker compose up
```

Otherwise

1. Install the runner
    ```
    docker run --rm code.forgejo.org/forgejo/runner:3.3.0 forgejo-runner --version
    ```
1. Save the action runner registration token to `TAILWINDCSS_FLUID_FONT_SIZE_ACTIONS_RUNNER_REGISTRATION_TOKEN` in your shell. Take steps to avoid the token being added to your shell history.
1. Generate the `.runner` config file. _⚠️ Replace `{NAME}` with any identifier not already taken by a runner registered to the repo.
    ```shell
    docker run -v /var/run/docker.sock:/var/run/docker.sock  -v $PWD:/data --rm code.forgejo.org/forgejo/runner:3.3.0 forgejo-runner register --no-interactive --token $TAILWINDCSS_FLUID_FONT_SIZE_ACTIONS_RUNNER_REGISTRATION_TOKEN --name {NAME} --instance https://codeberg.org
    ```
1. Start the runner
    ```shell
    docker compose up
    ```
