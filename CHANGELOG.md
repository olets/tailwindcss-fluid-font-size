# [3.0.0](https://codeberg.org/olets/tailwindcss-fluid-font-size/compare/v3.0.0-beta.3...v3.0.0) (2024-11-23)

⚠️ Has breaking changes. Read the 3.0.0-beta.x release notes below, and the [Migration guide](https://tailwindcss-fluid-font-size.olets.dev//migrating-between-versions).


# [3.0.0-beta.3](https://codeberg.org/olets/tailwindcss-fluid-font-size/compare/v3.0.0-beta.2...v3.0.0-beta.3) (2024-11-23)


* **package scripts:** rename publish-package from publish ([51ddb02](https://codeberg.org/olets/tailwindcss-fluid-font-size/commit/51ddb0269469c7416a6544300d61e209bfa7aaa8))

# [3.0.0-beta.2](https://codeberg.org/olets/tailwindcss-fluid-font-size/compare/v3.0.0-beta.1...v3.0.0-beta.2) (2024-11-23)


### Bug Fixes

* **package scripts:** use bun ([e2811e5](https://codeberg.org/olets/tailwindcss-fluid-font-size/commit/e2811e58b5809954e847a5a89167b49c7ecbf9f7))



# [3.0.0-beta.1](https://codeberg.org/olets/tailwindcss-fluid-font-size/compare/v2.1.4...v3.0.0-beta.1) (2024-11-23)

⚠️ Has breaking changes: no named export.

### Bug Fixes

* **tests:** add missing jest-diff dependency ([12764e3](https://codeberg.org/olets/tailwindcss-fluid-font-size/commit/12764e36e0398c3e6a7f96f9955ba153f0911c73))


### Features

* **build:** use rollup... ([503160c](https://codeberg.org/olets/tailwindcss-fluid-font-size/commit/503160c50fb739b82d2b9a1f0d4201b7c6949e5a))
* **distribution:** publish to npm ([0618921](https://codeberg.org/olets/tailwindcss-fluid-font-size/commit/0618921274d9d0b6cf022f9d5b30c86c6ea0536d))
* **exports:** drop named export to please rollup cjs support... ([0925e14](https://codeberg.org/olets/tailwindcss-fluid-font-size/commit/0925e1471c1ffeeabf6c61b1b9117b2c32212074))
* **jest,@jest/globals,@types/jest:** upgrade ([e8dfe53](https://codeberg.org/olets/tailwindcss-fluid-font-size/commit/e8dfe53c5fd56fbe767ac70000369f732a3f4064))
* **tailwindcss:** upgrade 3.3.2..3.4.14 ([602a7bc](https://codeberg.org/olets/tailwindcss-fluid-font-size/commit/602a7bc4dc1cc6f4cc870c9b192ab9469599cf3e))
* **tailwindcss:** upgrade 3.4.14..3.4.15 ([d51f1af](https://codeberg.org/olets/tailwindcss-fluid-font-size/commit/d51f1afcb6a4ce7b1028208c4ecc082127354ab2))



## [2.1.4](https://codeberg.org/olets/tailwindcss-fluid-font-size/compare/v2.1.3...v2.1.4) (2024-05-14)

Docs updates to play nice with JSR.

## [2.1.3](https://codeberg.org/olets/tailwindcss-fluid-font-size/compare/v2.1.2...v2.1.3) (2024-05-11)

Docs updates.


## [2.1.2](https://codeberg.org/olets/tailwindcss-fluid-font-size/compare/v2.1.1...v2.1.2) (2024-04-29)


### Features

* **tailwind:** pnpn lockfile uses 3.4.3 ([bddfb36](https://codeberg.org/olets/tailwindcss-fluid-font-size/commit/bddfb369549b554eb0667e507395a96bae7d5896))



## [2.1.1](https://codeberg.org/olets/tailwindcss-fluid-font-size/compare/v2.1.0...v2.1.1) (2024-04-29)


### Reverts

* pxNumber: use css-fluid-length's ([ee623d0](https://codeberg.org/olets/tailwindcss-fluid-font-size/commit/ee623d0eedfcfbc351d9fcd9d267664d3e1b86b5))



# [2.1.0](https://codeberg.org/olets/tailwindcss-fluid-font-size/compare/v2.0.0...v2.1.0) (2024-04-29)


### Features

* **pxNumber:** use css-fluid-length's. changes npm and ts config ([c8d7d05](https://codeberg.org/olets/tailwindcss-fluid-font-size/commit/c8d7d0506925945f0981a43971d4291013e378ea))



# [2.0.0](https://codeberg.org/olets/tailwindcss-fluid-font-size/compare/v1.4.0...v2.0.0) (2024-04-21)


### Features

* 'fontSize' not 'size', 'screen' not 'bound' ([a5a757f](https://codeberg.org/olets/tailwindcss-fluid-font-size/commit/a5a757f32ca02636192354718016d9896b20d294))



# [1.4.0](https://codeberg.org/olets/tailwindcss-fluid-font-size/compare/v1.3.0...v1.4.0) (2024-04-20)


### Features

* support utilities with bound and no size ([1def3ba](https://codeberg.org/olets/tailwindcss-fluid-font-size/commit/1def3bacaa3d9d641c94bf3b523ae6c507c28e01))



# [1.3.0](https://codeberg.org/olets/tailwindcss-fluid-font-size/compare/v1.2.2...v1.3.0) (2024-04-20)


### Features

* **default sizes:** add options fromSize, toSize, apply to :root ([87965c5](https://codeberg.org/olets/tailwindcss-fluid-font-size/commit/87965c552caaa05f6a66a269bc49cac45f85224c))



## [1.2.2](https://codeberg.org/olets/tailwindcss-fluid-font-size/compare/v1.2.1...v1.2.2) (2024-04-20)


### Bug Fixes

* **scripts:** typo ([1a05657](https://codeberg.org/olets/tailwindcss-fluid-font-size/commit/1a056572a025230e8db5f93c7594c498282a02c3))



## [1.2.1](https://codeberg.org/olets/tailwindcss-fluid-font-size/compare/v1.2.0...v1.2.1) (2024-04-20)


### Features

* **scripts:** support 'pnpm run changelog' ([d90932b](https://codeberg.org/olets/tailwindcss-fluid-font-size/commit/d90932b02508854decfe786deee8ac69a2020db9))
* **scripts:** support 'pnpm run publish' ([46bcf17](https://codeberg.org/olets/tailwindcss-fluid-font-size/commit/46bcf17287f12269cc575bfa8a21927191d4a86a))



## [1.2.0](https://codeberg.org/olets/tailwindcss-fluid-font-size/compare/1.1.0...1.2.0) (2024-04-20)


### Features

* **docs:** new docs site replaces most of the readme ([83f41a7](https://codeberg.org/olets/tailwindcss-fluid-font-size/commit/83f41a764abf447f693e8b33f12b715f7fc2b25e))
* **issue templates:** update for codeberg (no discussions) ([eafc93a](https://codeberg.org/olets/tailwindcss-fluid-font-size/commit/eafc93ae2a2186ea501f71df99ab7a5b04303e31))
* **warnings:** CSS '--*-warning' variables... ([bde74c2](https://codeberg.org/olets/tailwindcss-fluid-font-size/commit/bde74c27fbdbf4875b34859089f03a688fe25d20))

Depend on minimum working version of Tailwind.


## [1.1.0](https://codeberg.org/olets/tailwindcss-fluid-font-size/compare/1.0.1...1.1.0) (2024-04-02)

### Features

* **configuration**: bounds are options > theme > default theme ([ba84615](https://codeberg.org/olets/tailwindcss-fluid-font-size/commit/ba84615566cc91a0d60699d20cbae772e1c4b6a7))

## [1.0.1](https://codeberg.org/olets/tailwindcss-fluid-font-size/compare/1.0.0...1.0.1) (2024-03-24)

Add JSDoc to plugin to satisfy JSR score's "symbol docs"

## [1.0.0](https://codeberg.org/olets/tailwindcss-fluid-font-size/compare/1.0.0-beta.2...1.0.0) (2024-03-24)

Initial stable release 🎉


## [1.0.0-beta.2](https://codeberg.org/olets/tailwindcss-fluid-font-size/compare/1.0.0-beta.1...1.0.0-beta.2) (2024-03-17)


## [1.0.0-beta.1](https://codeberg.org/olets/tailwindcss-fluid-font-size/compare/1.0.0-alpha.3...1.0.0-beta.1) (2024-03-16)

## [1.0.0-alpha.3](https://codeberg.org/olets/tailwindcss-fluid-font-size/compare/1.0.0-alpha.2...1.0.0-alpha.3) (2024-03-14)

## [1.0.0-alpha.2](https://codeberg.org/olets/tailwindcss-fluid-font-size/compare/1.0.0-alpha.1...1.0.0-alpha.2) (2024-03-14)

## [1.0.0-alpha.1](https://codeberg.org/olets/tailwindcss-fluid-font-size/compare/fbf969652a52cb3ba291150a612fcfb3f1804f2a...1.0.0-alpha.1) (2024-03-13)

## [1.0.0-alpha.1](https://codeberg.org/olets/tailwindcss-fluid-font-size/compare/fbf969652a52cb3ba291150a612fcfb3f1804f2a...1.0.0-alpha.1) (2024-03-13)

