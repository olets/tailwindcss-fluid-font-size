# tailwindcss-fluid-font-size

A plugin for Tailwind CSS ^3.3.2 that provides utilities for fluid font sizes.

> 💗 Want fluid lengths for things other than font-size, including for non-Tailwind CSS-in-JS? Check out [css-fluid-length](https://codeberg.org/olets/css-fluid-length).

Add the plugin, and with no additional configuration you'll have utility classes for fluid typography going from any of Tailwind's font sizes between any of Tailwind's breakpoints, relative to window width or any other dimension. Customize it to add additional sizes and breakpoints.

Intellisense works, responsive variants work, and arbitrary classes work.

## Documentation

&nbsp;

📖 Visit the docs site at https://tailwindcss-fluid-font-size.olets.dev/

&nbsp;

Or dive right in.

```sh
# Install from NPM
# (recommended)
# by running one of
bun add -D @olets/tailwindcss-fluid-font-size
deno add npm:@olets/tailwindcss-fluid-font-size
npm add -D @olets/tailwindcss-fluid-font-size
pnpm add -D @olets/tailwindcss-fluid-font-size
yarn add -D @olets/tailwindcss-fluid-font-size

# Or install from JSR
# (deprecated, may be removed from JSR registry in a future major version)
# by running one of
bunx jsr add -D @olets/tailwindcss-fluid-font-size
deno add @olets/tailwindcss-fluid-font-size
npx jsr add -D @olets/tailwindcss-fluid-font-size
pnpm dlx jsr add -D @olets/tailwindcss-fluid-font-size
yarn dlx jsr add -D @olets/tailwindcss-fluid-font-size
```

```js
// Tailwind config
import fluidFontSizePlugin from "@olets/tailwindcss-fluid-font-size";
// …
  plugins: [ fluidFontSizePlugin ],
// …
```

## Forge

Star or fork the project, or read or open issues, at https://codeberg.org/olets/tailwindcss-fluid-font-size

## Changelog

See the [CHANGELOG](CHANGELOG.md) file.

## Contributing

Thanks for your interest. Contributions are welcome!

> Please note that this project is released with a [Contributor Code of Conduct](CODE_OF_CONDUCT.md). By participating in this project you agree to abide by its terms.

Check the [Issues](https://codeberg.org/olets/tailwindcss-fluid-font-size/issues) to see if your topic has been discussed before or if it is being worked on.

Please read [CONTRIBUTING.md](CONTRIBUTING.md) before opening a pull request.

## License

<a href="https://www.codeberg.org/olets/tailwindcss-fluid-font-size">tailwindcss-fluid-font-size</a> by <a href="https://www.codeberg.org/olets">Henry Bley-Vroman</a> is licensed under a license which is the unmodified text of <a href="https://creativecommons.org/licenses/by-nc-sa/4.0">CC BY-NC-SA 4.0</a> and the unmodified text of a <a href="https://firstdonoharm.dev/build?modules=eco,extr,media,mil,sv,usta">Hippocratic License 3</a>. It is not affiliated with Creative Commons or the Organization for Ethical Source.

Human-readable summary of (and not a substitute for) the [LICENSE](LICENSE) file:

You are free to

- Share — copy and redistribute the material in any medium or format
- Adapt — remix, transform, and build upon the material

Under the following terms

- Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
- Non-commercial — You may not use the material for commercial purposes.
- Ethics - You must abide by the ethical standards specified in the Hippocratic License 3 with Ecocide, Extractive Industries, US Tariff Act, Mass Surveillance, Military Activities, and Media modules.
- Preserve terms — If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.
- No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.
