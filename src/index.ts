import defaultTheme from 'tailwindcss/defaultTheme.js';
import plugin from 'tailwindcss/plugin.js';
import type { PluginAPI, ThemeConfig } from 'tailwindcss/types/config.d';

/**
 * A Tailwind CSS fluid font size plugin
 *
 * [Docs site]{@link https://tailwindcss-fluid-font-size.olets.dev/}.
 *
 * # Example
 *
 * ```ts
 * // tailwind config file
 *
 * import fluidFontSizePlugin from "@olets/tailwindcss-fluid-font-size";
 *
 * export default {
 *  // …
 *  plugins: [fluidFontSizePlugin],
 * }
 * ```
 *
 * ```html
 * <div class="text-fluid text-from-* text-to-*">…</div>
 * ```
 *
 * @module
 */

/**
 * Tailwind CSS fluid font size plugin
 *
 * For detailed documentation, see the docs site {@link https://tailwindcss-fluid-font-size.olets.dev/}
 *
 * ## Basic usage
 *
 * ```ts
 * // tailwind config file
 * import fluidFontSizePlugin from "@olets/tailwindcss-fluid-font-size";
 *
 * export default {
 *  // …
 *  plugins: [fluidFontSizePlugin],
 * }
 * ```
 *
 * ```html
 * <div class="text-fluid text-from-* text-to-*">…</div>
 * ```
 *
 * ## Customized
 *
 * Read more in the docs' Customization page {@link https://tailwindcss-fluid-font-size.olets.dev/customizing/options.html}
 *
 * ```ts
 * // tailwind config file
 * // …
 * export default {
 *   // …
 *   plugins: [
 *     fluidFontSize({
 *       fromFontSize: '4rem',
 *       fromScreen: '470px',
 *       relativeUnit: 'cqi',
 *       toFontSize: '7rem',
 *       toScreen: '740px',
 *     }),
 *   ]
 * }
 * ```
 *
 * @param {Options} options
 * @param {string} options.fromScreen
 * @param {boolean} options.pretty
 * @param {RelativeUnit} options.relativeUnit
 * @param {string} options.toScreen
 */
const fluidFontSize: PluginWithOptions = plugin.withOptions<Options>(
  (options = {}) => {
    return function ({
      addBase,
      addUtilities,
      matchUtilities,
      theme,
    }: PluginAPI) {
      configure(options, theme);

      createCssVariables(addBase);

      createFontSizeUtilities(addUtilities, matchUtilities, theme);

      createRelativeUnitUtilities(matchUtilities);
    };
  },
  () => {
    return {
      theme: {
        fontSizeFrom: {},
        fontSizeTo: {},
      },
    };
  },
);

/********************************************************************
 * FUNCTIONS
 *******************************************************************/

/**
 * Configure the plugin.
 *
 * @param {Options} options
 * @param {PluginAPI['theme']} theme
 */
function configure(options: Options = {}, theme: PluginAPI['theme']): void {
  CONFIGURATION = DEFAULT_CONFIGURATION;

  const themeFromScreen: string = theme('screens.sm') ?? '';
  const themeFromSize: string = theme('fontSize.base') ?? '';
  const themeToScreen: string = theme('screens.2xl') ?? '';
  const themeToSize: string = theme('fontSize.xl') ?? '';

  if (!isNaN(pxNumber(themeFromScreen))) {
    CONFIGURATION.fromScreen = themeFromScreen;
  }

  if (!isNaN(pxNumber(themeFromSize))) {
    CONFIGURATION.fromFontSize = themeFromSize;
  }

  if (!isNaN(pxNumber(themeToScreen))) {
    CONFIGURATION.toScreen = themeToScreen;
  }

  if (!isNaN(pxNumber(themeToSize))) {
    CONFIGURATION.toFontSize = themeToSize;
  }

  CONFIGURATION = {
    ...CONFIGURATION,
    ...options,
  };
}

/**
 * Create the CSS variables used by the utility classes.
 *
 * @param {PluginAPI['addBase']} addBase
 */
function createCssVariables(addBase: PluginAPI['addBase']): void {
  addBase({
    ':root': {
      '--fluid-font-size--from-screen-px': `${pxNumber(
        CONFIGURATION.fromScreen,
      )}`,
      '--fluid-font-size--from-font-size-px': `${pxNumber(
        CONFIGURATION.fromFontSize,
      )}`,
      '--fluid-font-size--relative-unit': `1${CONFIGURATION.relativeUnit}`,
      '--fluid-font-size--to-screen-px': `${pxNumber(CONFIGURATION.toScreen)}`,
      '--fluid-font-size--to-font-size-px': `${pxNumber(
        CONFIGURATION.toFontSize,
      )}`,
    },
  });
}

/**
 * Create `.text-fluid`, `.text-from-*`, `.text-to-*` utility classes.
 *
 * @param {PluginAPI['addUtilities']} addUtilities
 * @param {PluginAPI['matchUtilities']} matchUtilities
 * @param {PluginAPI['theme']} theme
 */
function createFontSizeUtilities(
  addUtilities: PluginAPI['addUtilities'],
  matchUtilities: PluginAPI['matchUtilities'],
  theme: PluginAPI['theme'],
): void {
  createSharedUtilities();
  createConfiguredUtilities();
  createArbitraryUtilities();

  /**
   * Create `.text-from-*` and `.text-to-*` utility classes
   * from arbitrary classes in the Tailwind content.
   */
  function createArbitraryUtilities(): void {
    matchUtilities({
      'text-from': (arbitraryValue: string): Css => {
        // DUPE text-from, text-to
        const [fontSize, screen]: PxableLength[] = arbitraryValue.split('@');

        const config: FluidFontSize = {
          fontSize:
            fontSize.length > 0
              ? theme(`fontSize.${fontSize}`) ?? fontSize
              : undefined,
          screen:
            screen === undefined
              ? screen
              : theme(`screens.${screen}`) ?? screen,
        };
        // end DUPE

        return getStyles('from', config, true);
      },
      'text-to': (arbitraryValue: string) => {
        // DUPE text-from, text-to
        const [fontSize, screen]: PxableLength[] = arbitraryValue.split('@');

        const config: FluidFontSize = {
          fontSize:
            fontSize.length > 0
              ? theme(`fontSize.${fontSize}`) ?? fontSize
              : undefined,
          screen:
            screen === undefined
              ? screen
              : theme(`screens.${screen}`) ?? screen,
        };
        // end DUPE

        return getStyles('to', config, true);
      },
    });
  }

  /**
   * Create `.text-from-*` and `.text-to-*` utility classes from the Tailwind theme config.
   */
  function createConfiguredUtilities(): void {
    const fromValues = getValues('fontSizeFrom', 'fontSizeFromScreens');
    for (const fromValue in fromValues) {
      const fromConfig = fromValues[fromValue];

      addUtilities({
        [`.text-from-${fromValue}`]: getStyles('from', fromConfig, false),
      });
    }

    const toValues = getValues('fontSizeTo', 'fontSizeToScreens');
    for (const toValue in toValues) {
      const toConfig = toValues[toValue];

      addUtilities({
        [`.text-to-${toValue}`]: getStyles('to', toConfig, false),
      });
    }

    /**
     * Extract the font size from a Tailwind font size value.
     *
     * @param {ThemeConfig['fontSize']} value A Tailwind font-size core plugin value.
     * @returns The font-size
     */
    function resolveTwFontSize(value: ThemeConfig['fontSize']): PxableLength {
      const [length] = Array.isArray(value) ? value : [value];

      return length;
    }

    /**
     * Builds a fluid font size config from
     * a Tailwind font size core plugin-like config's key
     * and a Tailwind screens core plugin-like config's key.
     *
     * @param {FontSizeThemeKey} fontSizeConfig a Tailwind font size core plugin-like config's key
     * @param {ScreensConfigKey} screensConfig a Tailwind screens core plugin-like config's key
     * @returns fluid font size config
     */
    function getValues(
      fontSizeConfig?: FontSizeThemeKey,
      screensConfig?: ScreensConfigKey,
    ): Values {
      const screens: PluginScreen = {};

      const configScreens: ThemeConfig['screens'] = {
        ...theme('screens'),
        ...theme('fontSizeScreens'),
        ...(screensConfig !== undefined ? theme(screensConfig) : {}),
      };

      const values: Values = {};

      const themeFontSize: object = theme('fontSize');

      Object.entries(themeFontSize).forEach(([key]) => {
        values[key] = { fontSize: theme(`fontSize.${key}`) };
      });

      if (fontSizeConfig !== undefined) {
        const themeFontSizeConfig: object = theme(fontSizeConfig);

        for (const [key, value] of Object.entries(themeFontSizeConfig)) {
          if (typeof value === 'object' && !Array.isArray(value)) {
            values[key] = {
              ...value,
              fontSize: resolveTwFontSize(
                value?.fontSize as ThemeConfig['fontSize'],
              ),
            };

            continue;
          }

          values[key] = {
            fontSize: resolveTwFontSize(value as ThemeConfig['fontSize']),
          };
        }
      }

      for (const screen in configScreens) {
        let value: string = '';
        let keys: string[] = [];
        let key: ScreenName | string = '';

        // @ts-expect-error @TODO
        value = configScreens[screen];

        switch (typeof value) {
          case 'string':
            if (value === '') {
              break;
            }

            screens[screen] = value;
            break;

          case 'object':
            keys = Object.keys(value);

            if (keys.length !== 1) {
              break;
            }

            key = keys[0];

            if (!SCREEN_NAMES.includes(key)) {
              break;
            }

            screens[screen] = value[key];

            break;
        }
      }

      for (const value of Object.keys(values)) {
        for (const screen in screens) {
          values[`\\@${screen}`] = {
            screen: screens[screen],
          };

          values[`${value}\\@${screen}`] = {
            fontSize: values[value].fontSize,
            screen: screens[screen],
          };
        }
      }

      return values;
    }
  }

  /**
   * Create the `.text-fluid` utility class.
   */
  function createSharedUtilities(): void {
    let textFluidStyles: Css = {};

    if (CONFIGURATION.pretty) {
      textFluidStyles = {
        '--fluid-font-size--slope':
          'calc(calc(var(--fluid-font-size--from-font-size-px) - var(--fluid-font-size--to-font-size-px)) / calc(var(--fluid-font-size--from-screen-px) - var(--fluid-font-size--to-screen-px)))',
        '--fluid-font-size--intercept-px':
          'calc(var(--fluid-font-size--from-font-size-px) - var(--fluid-font-size--slope) * var(--fluid-font-size--from-screen-px))',
        '--fluid-font-size--val': `calc(calc(100 * var(--fluid-font-size--relative-unit) * var(--fluid-font-size--slope)) + calc(1rem * var(--fluid-font-size--intercept-px) / ${ROOT_FONT_SIZE}))`,

        '--fluid-font-size--min': `calc(1rem * min(var(--fluid-font-size--to-font-size-px), var(--fluid-font-size--from-font-size-px)) / ${ROOT_FONT_SIZE})`,

        '--fluid-font-size--max': `calc(1rem * max(var(--fluid-font-size--to-font-size-px), var(--fluid-font-size--from-font-size-px)) / ${ROOT_FONT_SIZE})`,

        fontSize:
          'clamp(var(--fluid-font-size--min), var(--fluid-font-size--val), var(--fluid-font-size--max))',
      };
    } else {
      textFluidStyles = {
        fontSize: `clamp(
          calc(1rem * min(var(--fluid-font-size--to-font-size-px), var(--fluid-font-size--from-font-size-px)) / ${ROOT_FONT_SIZE}),
          calc(calc(100 * var(--fluid-font-size--relative-unit) * calc(calc(var(--fluid-font-size--from-font-size-px) - var(--fluid-font-size--to-font-size-px)) / calc(var(--fluid-font-size--from-screen-px) - var(--fluid-font-size--to-screen-px)))) + calc(1rem * calc(var(--fluid-font-size--from-font-size-px) - calc(calc(var(--fluid-font-size--from-font-size-px) - var(--fluid-font-size--to-font-size-px)) / calc(var(--fluid-font-size--from-screen-px) - var(--fluid-font-size--to-screen-px))) * var(--fluid-font-size--from-screen-px)) / ${ROOT_FONT_SIZE})),
          calc(1rem * max(var(--fluid-font-size--to-font-size-px), var(--fluid-font-size--from-font-size-px)) / ${ROOT_FONT_SIZE})
        )`,
      };
    }

    addUtilities({
      '.text-fluid': textFluidStyles,
    });
  }

  /**
   * Utility to generate the CSS for `.text-from-*`
   * and `.text-to-*` utility classes.
   *
   * @param {StopName} stop
   * @param {FluidFontSize} config
   * @param {boolean} logWarnings
   * @returns The CSS styles.
   */
  function getStyles(
    stop: StopName,
    config: FluidFontSize,
    logWarnings: boolean,
  ): Css {
    const { fontSize, screen } = config;

    const ret: Css = {};

    if (fontSize !== undefined) {
      const fontSizeNumber = pxNumber(fontSize);

      if (isNaN(fontSizeNumber)) {
        const fontSizeWarning: string = warnBadLength(
          `text-${stop} font-size`,
          fontSize,
          logWarnings,
        );

        ret[
          `--fluid-font-size--${stop}-font-size-px`
        ] = `${fontSize} /* ${fontSizeWarning} */`;

        ret[`--fluid-font-size--${stop}-font-size--warning`] =
          JSON.stringify(fontSizeWarning);
      } else {
        ret[`--fluid-font-size--${stop}-font-size-px`] = `${fontSizeNumber}`;
      }
    }

    if (screen !== undefined) {
      const screenNumber = pxNumber(screen);

      if (isNaN(screenNumber)) {
        const screenWarning: string = warnBadLength(
          `text-${stop} screen`,
          screen,
          logWarnings,
        );

        ret[
          `--fluid-font-size--${stop}-screen-px`
        ] = `${screen} /* ${screenWarning} */`;

        ret[`--fluid-font-size--${stop}-screen--warning`] =
          JSON.stringify(screenWarning);
      } else {
        ret[`--fluid-font-size--${stop}-screen-px`] = `${screenNumber}`;
      }
    }

    return ret;

    /**
     * Warns if the value is not a pixel or rem length.
     *
     * @param {string} caller The thing which received a bad value
     * @param {any} value The bad value
     * @param {boolean} log Whether to log the warning
     * @returns The warning message.
     */
    // eslint-disable-next-line  @typescript-eslint/no-explicit-any
    function warnBadLength(caller: string, value: any, log: boolean): string {
      const msg = `Warning: '${caller}' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem').`;

      if (log) {
        console.log(`${msg} Was passed '${value}'.`);
      }

      return msg;
    }
  }
}

/**
 * Create `.text-relative-unit-*` utility classes.
 *
 * @param {PluginAPI['matchUtilities']} matchUtilities
 */
function createRelativeUnitUtilities(
  matchUtilities: PluginAPI['matchUtilities'],
): void {
  matchUtilities(
    {
      'text-relative-unit': (value: RelativeUnit | string): Css => ({
        '--fluid-font-size--relative-unit': `1${value}`,
      }),
    },
    {
      values: RELATIVE_UNITS.reduce<Record<string, RelativeUnit>>(
        (acc, cur) => {
          acc[cur] = cur as RelativeUnit;
          return acc;
        },
        {},
      ),
    },
  );
}

/**
 * Converts a pixel or rem length to a number of pixels. Unitless length inputs are treated as pixel lengths.
 *
 * @param {PxableLength} length - Pixel value (e.g. `"1px"`), rem value (e.g. `"1rem"`), or number-like value (`"1"`)
 * @returns number of pixels.
 */
function pxNumber(length: PxableLength): number {
  let matches: RegExpMatchArray | null;

  if (JSON.stringify(Number(length)) === length) {
    return Number(length);
  }

  if (Array.isArray(length)) {
    // Support Tailwind's core fontSize plugin's array syntax.
    matches = length[0].match(PXABLE_LENGTH_PATTERN);
  } else {
    matches = length.match(PXABLE_LENGTH_PATTERN);
  }

  if (matches === null) {
    // input is not really a PxableLength (see PxableLength comment)
    return NaN;
  }

  const [value, unit] = matches.slice(1) as [string, string];

  if (!PXABLE_UNITS.includes(unit)) {
    return NaN;
  }

  if (unit === 'rem') {
    return Number(value) * ROOT_FONT_SIZE;
  }

  return Number(value);
}

/********************************************************************
 * VARIABLES
 *******************************************************************/

const SCREEN_NAMES = ['min', 'max'] satisfies ScreenName[] as string[];

const DEFAULT_CONFIGURATION: WithAllRequired<Options> = {
  fromFontSize: defaultTheme.fontSize.base[0],
  fromScreen: defaultTheme.screens.sm,
  pretty: true,
  relativeUnit: 'vw',
  toFontSize: defaultTheme.fontSize.xl[0],
  toScreen: defaultTheme.screens['2xl'],
};

let CONFIGURATION: typeof DEFAULT_CONFIGURATION = DEFAULT_CONFIGURATION;

const PXABLE_UNITS = ['px', 'rem'] satisfies PxableUnit[] as string[];

// @TODO refactor if https://github.com/microsoft/TypeScript/issues/41160 lands
const PXABLE_LENGTH_PATTERN = new RegExp(
  `(^\\d*\\.{0,1}\\d+)(${PXABLE_UNITS.join('|')})$`,
);

const RELATIVE_UNITS = [
  // container
  'cqb',
  'cqh',
  'cqi',
  'cqmax',
  'cqmin',
  'cqw',
  // local font
  'cap',
  'ch',
  'em',
  'ex',
  'ic',
  'lh',
  // root font
  'rcap',
  'rch',
  'rem',
  'rex',
  'ric',
  'rlh',
  // viewport
  'dvh',
  'dvw',
  'lvh',
  'lvw',
  'svh',
  'svw',
  'vb',
  'vh',
  'vi',
  'vmin',
  'vmax',
  'vw',
] satisfies RelativeUnit[] as string[];

const ROOT_FONT_SIZE = 16;

/********************************************************************
 * EXPORTS
 *******************************************************************/

export { fluidFontSize as default };
