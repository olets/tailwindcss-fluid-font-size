type ScreenName = 'min' | 'max';

type PluginScreen = Record<string, string>;

type Css = Record<string, string>;

type Values = Record<string, FluidFontSize>;

type FluidFontSize = {
  fontSize?: PxableLength;
  screen?: PxableLength;
};

type FontSizeThemeKey = 'fontSizeFrom' | 'fontSizeTo';

type Options = {
  fromFontSize?: PxableLength;
  fromScreen?: PxableLength;
  pretty?: boolean;
  relativeUnit?: RelativeUnit;
  toFontSize?: PxableLength;
  toScreen?: PxableLength;
};

// Compare to Tailwind's PluginsConfig type
type PluginWithOptions = {
  (options: Options): { handler: PluginCreator; config?: Partial<Config> };
  __isOptionsFunction: true;
};

// Really `"numeric string" or "string matching PXABLE_LENGTH_PATTERN"`
type PxableLength = string;

type PxableUnit = 'px' | 'rem';

type RelativeUnit =
  // container
  | 'cqb'
  | 'cqh'
  | 'cqi'
  | 'cqmax'
  | 'cqmin'
  | 'cqw'
  // local font
  | 'cap'
  | 'ch'
  | 'em'
  | 'ex'
  | 'ic'
  | 'lh'
  // root font
  | 'rcap'
  | 'rch'
  | 'rem'
  | 'rex'
  | 'ric'
  | 'rlh'
  // viewport
  | 'dvh'
  | 'dvw'
  | 'lvh'
  | 'lvw'
  | 'svh'
  | 'svw'
  | 'vb'
  | 'vh'
  | 'vi'
  | 'vmin'
  | 'vmax'
  | 'vw';

type ScreensConfigKey = 'fontSizeFromScreens' | 'fontSizeToScreens';

type StopName = 'from' | 'to';

type WithAllRequired<T> = {
  [P in keyof T]-?: T[P];
};
