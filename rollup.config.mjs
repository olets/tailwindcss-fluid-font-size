//@ts-check

import { nodeResolve } from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import dts from 'rollup-plugin-dts';
import esbuild from 'rollup-plugin-esbuild';
import terser from '@rollup/plugin-terser';
import rollupReplace from '@rollup/plugin-replace';
import { defineConfig } from 'rollup';

const replace = (opts) => {
  return rollupReplace({
    ...opts,
    preventAssignment: true,
  });
};

export default defineConfig([
  {
    input: 'src/index.ts',
    output: [
      {
        file: 'dist/index.js',
        format: 'cjs',
      },
      {
        file: 'dist/index.esm.js',
        format: 'esm',
      },
    ],
    plugins: [
      replace({
        __CDN_ROOT__: '',
        __BROWSER__: JSON.stringify(false),
      }),
      esbuild(),
      nodeResolve(),
      commonjs(),
    ],
  },
  {
    input: 'src/index.ts',
    output: {
      file: 'dist/index.browser.mjs',
      format: 'esm',
    },
    plugins: [
      replace({
        __BROWSER__: JSON.stringify(true),
        __CDN_ROOT__: '',
      }),
      esbuild(),
      nodeResolve(),
      commonjs(),
      terser(),
    ],
  },
  {
    input: 'src/index.ts',
    output: [
      {
        file: 'dist/index.d.ts',
        format: 'es',
      },
    ],
    plugins: [dts()],
    onwarn: (warning, warn) => {
      if (!/Circular/.test(warning.message)) {
        warn(warning);
      }
    },
  },
]);
