import { html, css, run } from './run';
import fluidFontSizePlugin from '../src';
import { expect } from '@jest/globals';
import containerQueriesPlugin from '@tailwindcss/container-queries';
import type { PluginAPI } from 'tailwindcss/types/config.d';

type Theme = PluginAPI['theme'];

it('should support consuming import by default', async () => {
  const config = {
    content: [
      {
        raw: html`
          <div>
            <div class="text-fluid"></div>
          </div>
        `,
      },
    ],
  };

  const input = css`
    @tailwind base;
  `;

  await run(input, config).then((result) => {
    // @ts-expect-error ts(2339)
    expect(result.css).toMatchFormattedCss(css`
      ${
        /**
         * start Tailwind's @base layer
         */
        ''
      }
      *,
      ::before,
      ::after {
        --tw-border-spacing-x: 0;
        --tw-border-spacing-y: 0;
        --tw-translate-x: 0;
        --tw-translate-y: 0;
        --tw-rotate: 0;
        --tw-skew-x: 0;
        --tw-skew-y: 0;
        --tw-scale-x: 1;
        --tw-scale-y: 1;
        --tw-pan-x: ;
        --tw-pan-y: ;
        --tw-pinch-zoom: ;
        --tw-scroll-snap-strictness: proximity;
        --tw-gradient-from-position: ;
        --tw-gradient-via-position: ;
        --tw-gradient-to-position: ;
        --tw-ordinal: ;
        --tw-slashed-zero: ;
        --tw-numeric-figure: ;
        --tw-numeric-spacing: ;
        --tw-numeric-fraction: ;
        --tw-ring-inset: ;
        --tw-ring-offset-width: 0px;
        --tw-ring-offset-color: #fff;
        --tw-ring-color: rgb(59 130 246 / 0.5);
        --tw-ring-offset-shadow: 0 0 #0000;
        --tw-ring-shadow: 0 0 #0000;
        --tw-shadow: 0 0 #0000;
        --tw-shadow-colored: 0 0 #0000;
        --tw-blur: ;
        --tw-brightness: ;
        --tw-contrast: ;
        --tw-grayscale: ;
        --tw-hue-rotate: ;
        --tw-invert: ;
        --tw-saturate: ;
        --tw-sepia: ;
        --tw-drop-shadow: ;
        --tw-backdrop-blur: ;
        --tw-backdrop-brightness: ;
        --tw-backdrop-contrast: ;
        --tw-backdrop-grayscale: ;
        --tw-backdrop-hue-rotate: ;
        --tw-backdrop-invert: ;
        --tw-backdrop-opacity: ;
        --tw-backdrop-saturate: ;
        --tw-backdrop-sepia: ;
        --tw-contain-size: ;
        --tw-contain-layout: ;
        --tw-contain-paint: ;
        --tw-contain-style: ;
      }
      ::backdrop {
        --tw-border-spacing-x: 0;
        --tw-border-spacing-y: 0;
        --tw-translate-x: 0;
        --tw-translate-y: 0;
        --tw-rotate: 0;
        --tw-skew-x: 0;
        --tw-skew-y: 0;
        --tw-scale-x: 1;
        --tw-scale-y: 1;
        --tw-pan-x: ;
        --tw-pan-y: ;
        --tw-pinch-zoom: ;
        --tw-scroll-snap-strictness: proximity;
        --tw-gradient-from-position: ;
        --tw-gradient-via-position: ;
        --tw-gradient-to-position: ;
        --tw-ordinal: ;
        --tw-slashed-zero: ;
        --tw-numeric-figure: ;
        --tw-numeric-spacing: ;
        --tw-numeric-fraction: ;
        --tw-ring-inset: ;
        --tw-ring-offset-width: 0px;
        --tw-ring-offset-color: #fff;
        --tw-ring-color: rgb(59 130 246 / 0.5);
        --tw-ring-offset-shadow: 0 0 #0000;
        --tw-ring-shadow: 0 0 #0000;
        --tw-shadow: 0 0 #0000;
        --tw-shadow-colored: 0 0 #0000;
        --tw-blur: ;
        --tw-brightness: ;
        --tw-contrast: ;
        --tw-grayscale: ;
        --tw-hue-rotate: ;
        --tw-invert: ;
        --tw-saturate: ;
        --tw-sepia: ;
        --tw-drop-shadow: ;
        --tw-backdrop-blur: ;
        --tw-backdrop-brightness: ;
        --tw-backdrop-contrast: ;
        --tw-backdrop-grayscale: ;
        --tw-backdrop-hue-rotate: ;
        --tw-backdrop-invert: ;
        --tw-backdrop-opacity: ;
        --tw-backdrop-saturate: ;
        --tw-backdrop-sepia: ;
        --tw-contain-size: ;
        --tw-contain-layout: ;
        --tw-contain-paint: ;
        --tw-contain-style: ;
      }
      ${
        /**
         * end Tailwind's @base layer
         */
        ''
      }
      :root {
        --fluid-font-size--from-screen-px: 640;
        --fluid-font-size--from-font-size-px: 16;
        --fluid-font-size--relative-unit: 1vw;
        --fluid-font-size--to-screen-px: 1536;
        --fluid-font-size--to-font-size-px: 20;
      }
    `);
  });
});

it('should add the :root styles', async () => {
  const config = {
    content: [
      {
        raw: html`
          <div>
            <div class="text-fluid"></div>
          </div>
        `,
      },
    ],
  };

  const input = css`
    @tailwind base;
  `;

  await run(input, config).then((result) => {
    // @ts-expect-error ts(2339)
    expect(result.css).toMatchFormattedCss(css`
      ${
        /**
         * start Tailwind's @base layer
         */
        ''
      }
      *,
      ::before,
      ::after {
        --tw-border-spacing-x: 0;
        --tw-border-spacing-y: 0;
        --tw-translate-x: 0;
        --tw-translate-y: 0;
        --tw-rotate: 0;
        --tw-skew-x: 0;
        --tw-skew-y: 0;
        --tw-scale-x: 1;
        --tw-scale-y: 1;
        --tw-pan-x: ;
        --tw-pan-y: ;
        --tw-pinch-zoom: ;
        --tw-scroll-snap-strictness: proximity;
        --tw-gradient-from-position: ;
        --tw-gradient-via-position: ;
        --tw-gradient-to-position: ;
        --tw-ordinal: ;
        --tw-slashed-zero: ;
        --tw-numeric-figure: ;
        --tw-numeric-spacing: ;
        --tw-numeric-fraction: ;
        --tw-ring-inset: ;
        --tw-ring-offset-width: 0px;
        --tw-ring-offset-color: #fff;
        --tw-ring-color: rgb(59 130 246 / 0.5);
        --tw-ring-offset-shadow: 0 0 #0000;
        --tw-ring-shadow: 0 0 #0000;
        --tw-shadow: 0 0 #0000;
        --tw-shadow-colored: 0 0 #0000;
        --tw-blur: ;
        --tw-brightness: ;
        --tw-contrast: ;
        --tw-grayscale: ;
        --tw-hue-rotate: ;
        --tw-invert: ;
        --tw-saturate: ;
        --tw-sepia: ;
        --tw-drop-shadow: ;
        --tw-backdrop-blur: ;
        --tw-backdrop-brightness: ;
        --tw-backdrop-contrast: ;
        --tw-backdrop-grayscale: ;
        --tw-backdrop-hue-rotate: ;
        --tw-backdrop-invert: ;
        --tw-backdrop-opacity: ;
        --tw-backdrop-saturate: ;
        --tw-backdrop-sepia: ;
        --tw-contain-size: ;
        --tw-contain-layout: ;
        --tw-contain-paint: ;
        --tw-contain-style: ;
      }
      ::backdrop {
        --tw-border-spacing-x: 0;
        --tw-border-spacing-y: 0;
        --tw-translate-x: 0;
        --tw-translate-y: 0;
        --tw-rotate: 0;
        --tw-skew-x: 0;
        --tw-skew-y: 0;
        --tw-scale-x: 1;
        --tw-scale-y: 1;
        --tw-pan-x: ;
        --tw-pan-y: ;
        --tw-pinch-zoom: ;
        --tw-scroll-snap-strictness: proximity;
        --tw-gradient-from-position: ;
        --tw-gradient-via-position: ;
        --tw-gradient-to-position: ;
        --tw-ordinal: ;
        --tw-slashed-zero: ;
        --tw-numeric-figure: ;
        --tw-numeric-spacing: ;
        --tw-numeric-fraction: ;
        --tw-ring-inset: ;
        --tw-ring-offset-width: 0px;
        --tw-ring-offset-color: #fff;
        --tw-ring-color: rgb(59 130 246 / 0.5);
        --tw-ring-offset-shadow: 0 0 #0000;
        --tw-ring-shadow: 0 0 #0000;
        --tw-shadow: 0 0 #0000;
        --tw-shadow-colored: 0 0 #0000;
        --tw-blur: ;
        --tw-brightness: ;
        --tw-contrast: ;
        --tw-grayscale: ;
        --tw-hue-rotate: ;
        --tw-invert: ;
        --tw-saturate: ;
        --tw-sepia: ;
        --tw-drop-shadow: ;
        --tw-backdrop-blur: ;
        --tw-backdrop-brightness: ;
        --tw-backdrop-contrast: ;
        --tw-backdrop-grayscale: ;
        --tw-backdrop-hue-rotate: ;
        --tw-backdrop-invert: ;
        --tw-backdrop-opacity: ;
        --tw-backdrop-saturate: ;
        --tw-backdrop-sepia: ;
        --tw-contain-size: ;
        --tw-contain-layout: ;
        --tw-contain-paint: ;
        --tw-contain-style: ;
      }
      ${
        /**
         * end Tailwind's @base layer
         */
        ''
      }
      :root {
        --fluid-font-size--from-screen-px: 640;
        --fluid-font-size--from-font-size-px: 16;
        --fluid-font-size--relative-unit: 1vw;
        --fluid-font-size--to-screen-px: 1536;
        --fluid-font-size--to-font-size-px: 20;
      }
    `);
  });
});

it('should add the .text-fluid utility', async () => {
  const config = {
    content: [
      {
        raw: html`
          <div>
            <div class="text-fluid"></div>
          </div>
        `,
      },
    ],
  };

  const input = css`
    @tailwind utilities;
  `;

  await run(input, config).then((result) => {
    // @ts-expect-error ts(2339)
    expect(result.css).toMatchFormattedCss(css`
      .text-fluid {
        --fluid-font-size--slope: calc(
          calc(
              var(--fluid-font-size--from-font-size-px) -
                var(--fluid-font-size--to-font-size-px)
            ) /
            calc(
              var(--fluid-font-size--from-screen-px) -
                var(--fluid-font-size--to-screen-px)
            )
        );
        --fluid-font-size--intercept-px: calc(
          var(--fluid-font-size--from-font-size-px) -
            var(--fluid-font-size--slope) *
            var(--fluid-font-size--from-screen-px)
        );
        --fluid-font-size--val: calc(
          calc(
              100 * var(--fluid-font-size--relative-unit) *
                var(--fluid-font-size--slope)
            ) + calc(1rem * var(--fluid-font-size--intercept-px) / 16)
        );
        --fluid-font-size--min: calc(
          1rem *
            min(
              var(--fluid-font-size--to-font-size-px),
              var(--fluid-font-size--from-font-size-px)
            ) / 16
        );
        --fluid-font-size--max: calc(
          1rem *
            max(
              var(--fluid-font-size--to-font-size-px),
              var(--fluid-font-size--from-font-size-px)
            ) / 16
        );
        font-size: clamp(
          var(--fluid-font-size--min),
          var(--fluid-font-size--val),
          var(--fluid-font-size--max)
        );
      }
    `);
  });
});

it('should add configured text-(from|to)-<size> utilities', async () => {
  const config = {
    content: [
      {
        raw: html`
          <div>
            <div class="text-from-xs text-to-9xl"></div>
            <div class="text-from-4xl text-to-xs"></div>
          </div>
        `,
      },
    ],
  };

  const input = css`
    @tailwind utilities;
  `;

  await run(input, config).then((result) => {
    // @ts-expect-error ts(2339)
    expect(result.css).toMatchFormattedCss(css`
      .text-from-xs {
        --fluid-font-size--from-font-size-px: 12;
      }

      .text-from-4xl {
        --fluid-font-size--from-font-size-px: 36;
      }

      .text-to-xs {
        --fluid-font-size--to-font-size-px: 12;
      }

      .text-to-9xl {
        --fluid-font-size--to-font-size-px: 128;
      }
    `);
  });
});

it('should add configured text-(from|to)-@<screen> utilities', async () => {
  const config = {
    content: [
      {
        raw: html`
          <div>
            <div class="text-from-@sm text-to-@xl"></div>
          </div>
        `,
      },
    ],
  };

  const input = css`
    @tailwind utilities;
  `;

  await run(input, config).then((result) => {
    // @ts-expect-error ts(2339)
    expect(result.css).toMatchFormattedCss(css`
      .text-from-\@sm {
        --fluid-font-size--from-screen-px: 640;
      }
      .text-to-\@xl {
        --fluid-font-size--to-screen-px: 1280;
      }
    `);
  });
});

it('should add configured text-(from|to)-<size>@<screen> utilities', async () => {
  const config = {
    content: [
      {
        raw: html`
          <div>
            <div class="text-from-sm@lg text-to-3xl@xl"></div>
            <div class="text-from-custom@lg"></div>
            <div
              class="text-from-sm@min text-to-3xl@minObj text-from-sm@max"
            ></div>
          </div>
        `,
      },
    ],
    theme: {
      extend: {
        screens: {
          min: '47px',
          minObj: { min: '474px' },
          max: { max: '4747px' },
          raw: { raw: '47474px' },
          two: { min: '474747px', max: '4747474px' },
        },
      },
      fontSizeFrom: {
        custom: '10px',
      },
    },
  };

  const input = css`
    @tailwind utilities;
  `;

  await run(input, config).then((result) => {
    // @ts-expect-error ts(2339)
    expect(result.css).toMatchFormattedCss(css`
      .text-from-sm\@lg {
        --fluid-font-size--from-font-size-px: 14;
        --fluid-font-size--from-screen-px: 1024;
      }
      .text-from-sm\@min {
        --fluid-font-size--from-font-size-px: 14;
        --fluid-font-size--from-screen-px: 47;
      }
      .text-from-sm\@max {
        --fluid-font-size--from-font-size-px: 14;
        --fluid-font-size--from-screen-px: 4747;
      }
      .text-from-custom\@lg {
        --fluid-font-size--from-font-size-px: 10;
        --fluid-font-size--from-screen-px: 1024;
      }
      .text-to-3xl\@xl {
        --fluid-font-size--to-font-size-px: 30;
        --fluid-font-size--to-screen-px: 1280;
      }
      .text-to-3xl\@minObj {
        --fluid-font-size--to-font-size-px: 30;
        --fluid-font-size--to-screen-px: 474;
      }
    `);
  });
});

it('should add arbitrary text-(from|to)-[<size>] utilities', async () => {
  const config = {
    content: [
      {
        raw: html`
          <div>
            <div class="text-from-[30px] text-to-[4rem]"></div>
            <div class="text-from-[30px] text-to-[4rem]"></div>
          </div>
        `,
      },
    ],
  };

  const input = css`
    @tailwind utilities;
  `;

  await run(input, config).then((result) => {
    // @ts-expect-error ts(2339)
    expect(result.css).toMatchFormattedCss(css`
      .text-from-\[30px\] {
        --fluid-font-size--from-font-size-px: 30;
      }

      .text-to-\[4rem\] {
        --fluid-font-size--to-font-size-px: 64;
      }
    `);
  });
});

it('should add arbitrary text-(from|to)-[@<screen>] utilities', async () => {
  const config = {
    content: [
      {
        raw: html`
          <div>
            <div class="text-from-[@30px] text-to-[@4rem]"></div>
          </div>
        `,
      },
    ],
  };

  const input = css`
    @tailwind utilities;
  `;

  await run(input, config).then((result) => {
    // @ts-expect-error ts(2339)
    expect(result.css).toMatchFormattedCss(css`
      .text-from-\[\@30px\] {
        --fluid-font-size--from-screen-px: 30;
      }
      .text-to-\[\@4rem\] {
        --fluid-font-size--to-screen-px: 64;
      }
    `);
  });
});

it('should add arbitrary text-(from|to)-[<size>@<screen>] utilities', async () => {
  const config = {
    content: [
      {
        raw: html`
          <div>
            <div class="text-from-[sm@400px] text-to-[3xl@40rem]"></div>
            <div class="text-from-[10px@md] text-to-[3rem@lg]"></div>
          </div>
        `,
      },
    ],
    theme: {
      extend: {
        screens: {
          min: '47px',
          minObj: { min: '474px' },
          max: { max: '4747px' },
          raw: { raw: '47474px' },
          two: { min: '474747px', max: '4747474px' },
        },
      },
      fontSizeFrom: {
        custom: '10px',
      },
    },
  };

  const input = css`
    @tailwind utilities;
  `;

  await run(input, config).then((result) => {
    // @ts-expect-error ts(2339)
    expect(result.css).toMatchFormattedCss(css`
      .text-from-\[10px\@md\] {
        --fluid-font-size--from-font-size-px: 10;
        --fluid-font-size--from-screen-px: 768;
      }
      .text-from-\[sm\@400px\] {
        --fluid-font-size--from-font-size-px: 14;
        --fluid-font-size--from-screen-px: 400;
      }
      .text-to-\[3rem\@lg\] {
        --fluid-font-size--to-font-size-px: 48;
        --fluid-font-size--to-screen-px: 1024;
      }
      .text-to-\[3xl\@40rem\] {
        --fluid-font-size--to-font-size-px: 30;
        --fluid-font-size--to-screen-px: 640;
      }
    `);
  });
});

it('should add text-(from|to)-<config> utilities', async () => {
  const config = {
    content: [
      {
        raw: html`
          <div
            class="text-from-font-size-and-screen-from text-from-font-size-and-screen-from@lg text-from-font-size-from text-from-screen-from"
          ></div>
          <div
            class="text-to-font-size-and-screen-to text-to-font-size-and-screen-to@lg text-to-font-size-to text-to-screen-to"
          ></div>
        `,
      },
    ],
    theme: {
      fontSizeFrom: {
        'font-size-and-screen-from': {
          fontSize: '1px',
          screen: '2px',
        },
        'font-size-from': '3px',
        'screen-from': '4px',
      },
      fontSizeTo: {
        'font-size-and-screen-to': {
          fontSize: '5px',
          screen: '6px',
        },
        'font-size-to': '7px',
        'screen-to': '8px',
      },
    },
  };

  const input = css`
    @tailwind utilities;
  `;

  await run(input, config).then((result) => {
    // @ts-expect-error ts(2339)
    expect(result.css).toMatchFormattedCss(css`
      .text-from-font-size-and-screen-from {
        --fluid-font-size--from-font-size-px: 1;
        --fluid-font-size--from-screen-px: 2;
      }
      .text-from-font-size-from {
        --fluid-font-size--from-font-size-px: 3;
      }
      .text-from-screen-from {
        --fluid-font-size--from-font-size-px: 4;
      }
      .text-from-font-size-and-screen-from\@lg {
        --fluid-font-size--from-font-size-px: 1;
        --fluid-font-size--from-screen-px: 1024;
      }
      .text-to-font-size-and-screen-to {
        --fluid-font-size--to-font-size-px: 5;
        --fluid-font-size--to-screen-px: 6;
      }
      .text-to-font-size-to {
        --fluid-font-size--to-font-size-px: 7;
      }
      .text-to-screen-to {
        --fluid-font-size--to-font-size-px: 8;
      }
      .text-to-font-size-and-screen-to\@lg {
        --fluid-font-size--to-font-size-px: 5;
        --fluid-font-size--to-screen-px: 1024;
      }
    `);
  });
});

it('should add text-relative-unit-<unit> utilities', async () => {
  const config = {
    content: [
      {
        raw: html`<div
          class="
            text-relative-unit-cqb
            text-relative-unit-cqh
            text-relative-unit-cqi
            text-relative-unit-cqmax
            text-relative-unit-cqmin
            text-relative-unit-cqw
            text-relative-unit-cap
            text-relative-unit-ch
            text-relative-unit-em
            text-relative-unit-ex
            text-relative-unit-ic
            text-relative-unit-lh
            text-relative-unit-rcap
            text-relative-unit-rch
            text-relative-unit-rem
            text-relative-unit-rex
            text-relative-unit-ric
            text-relative-unit-rlh
            text-relative-unit-dvh
            text-relative-unit-dvw
            text-relative-unit-lvh
            text-relative-unit-lvw
            text-relative-unit-svh
            text-relative-unit-svw
            text-relative-unit-vb
            text-relative-unit-vh
            text-relative-unit-vi
            text-relative-unit-vmin
            text-relative-unit-vmax
            text-relative-unit-vw
          "
        ></div> `,
      },
    ],
  };

  const input = css`
    @tailwind utilities;
  `;

  await run(input, config).then((result) => {
    // @ts-expect-error ts(2339)
    expect(result.css).toMatchFormattedCss(css`
      .text-relative-unit-cap {
        --fluid-font-size--relative-unit: 1cap;
      }
      .text-relative-unit-ch {
        --fluid-font-size--relative-unit: 1ch;
      }
      .text-relative-unit-cqb {
        --fluid-font-size--relative-unit: 1cqb;
      }
      .text-relative-unit-cqh {
        --fluid-font-size--relative-unit: 1cqh;
      }
      .text-relative-unit-cqi {
        --fluid-font-size--relative-unit: 1cqi;
      }
      .text-relative-unit-cqmax {
        --fluid-font-size--relative-unit: 1cqmax;
      }
      .text-relative-unit-cqmin {
        --fluid-font-size--relative-unit: 1cqmin;
      }
      .text-relative-unit-cqw {
        --fluid-font-size--relative-unit: 1cqw;
      }
      .text-relative-unit-dvh {
        --fluid-font-size--relative-unit: 1dvh;
      }
      .text-relative-unit-dvw {
        --fluid-font-size--relative-unit: 1dvw;
      }
      .text-relative-unit-em {
        --fluid-font-size--relative-unit: 1em;
      }
      .text-relative-unit-ex {
        --fluid-font-size--relative-unit: 1ex;
      }
      .text-relative-unit-ic {
        --fluid-font-size--relative-unit: 1ic;
      }
      .text-relative-unit-lh {
        --fluid-font-size--relative-unit: 1lh;
      }
      .text-relative-unit-lvh {
        --fluid-font-size--relative-unit: 1lvh;
      }
      .text-relative-unit-lvw {
        --fluid-font-size--relative-unit: 1lvw;
      }
      .text-relative-unit-rcap {
        --fluid-font-size--relative-unit: 1rcap;
      }
      .text-relative-unit-rch {
        --fluid-font-size--relative-unit: 1rch;
      }
      .text-relative-unit-rem {
        --fluid-font-size--relative-unit: 1rem;
      }
      .text-relative-unit-rex {
        --fluid-font-size--relative-unit: 1rex;
      }
      .text-relative-unit-ric {
        --fluid-font-size--relative-unit: 1ric;
      }
      .text-relative-unit-rlh {
        --fluid-font-size--relative-unit: 1rlh;
      }
      .text-relative-unit-svh {
        --fluid-font-size--relative-unit: 1svh;
      }
      .text-relative-unit-svw {
        --fluid-font-size--relative-unit: 1svw;
      }
      .text-relative-unit-vb {
        --fluid-font-size--relative-unit: 1vb;
      }
      .text-relative-unit-vh {
        --fluid-font-size--relative-unit: 1vh;
      }
      .text-relative-unit-vi {
        --fluid-font-size--relative-unit: 1vi;
      }
      .text-relative-unit-vmax {
        --fluid-font-size--relative-unit: 1vmax;
      }
      .text-relative-unit-vmin {
        --fluid-font-size--relative-unit: 1vmin;
      }
      .text-relative-unit-vw {
        --fluid-font-size--relative-unit: 1vw;
      }
    `);
  });
});

it('should add utilities using custom screens', async () => {
  const config = {
    content: [
      {
        raw: html`
          <div>
            <div class="text-from-sm@screens text-to-sm@screens"></div>
            <div class="text-from-sm@screensMin text-to-sm@screensMin"></div>
            <div class="text-from-sm@screensMax text-to-sm@screensMax"></div>

            <div
              class="text-from-sm@fontSizeScreens text-to-sm@fontSizeScreens"
            ></div>
            <div
              class="text-from-sm@fontSizeScreensMin text-to-sm@fontSizeScreensMin"
            ></div>
            <div
              class="text-from-sm@fontSizeScreensMax text-to-sm@fontSizeScreensMax"
            ></div>
            <div class="text-from-sm@fontSizeScreensScreen"></div>

            <div
              class="text-from-sm@fontSizeFromScreens text-to-sm@fontSizeFromScreens"
            ></div>
            <div
              class="text-from-sm@fontSizeFromScreensMin text-to-sm@fontSizeFromScreensMin"
            ></div>
            <div
              class="text-from-sm@fontSizeFromScreensMax text-to-sm@fontSizeFromScreensMax"
            ></div>
            <div class="text-from-sm@fontSizeFromScreensScreen"></div>

            <div
              class="text-from-sm@fontSizeToScreens text-to-sm@fontSizeToScreens"
            ></div>
            <div
              class="text-from-sm@fontSizeToScreensMin text-to-sm@fontSizeToScreensMin"
            ></div>
            <div
              class="text-from-sm@fontSizeToScreensMax text-to-sm@fontSizeToScreensMax"
            ></div>
            <div class="text-to-sm@fontSizeToScreensScreen"></div>
          </div>
        `,
      },
    ],
    theme: {
      extend: {
        screens: {
          screens: '47px',
          screensMin: { min: '470px' },
          screensMax: { max: '470px' },
        },
      },
      fontSizeScreens: ({ theme }: { theme: Theme }) => ({
        fontSizeScreens: '47px',
        fontSizeScreensMin: { min: '470px' },
        fontSizeScreensMax: { max: '470px' },
        fontSizeScreensScreen: theme('screens.xl'),
      }),
      fontSizeFromScreens: ({ theme }: { theme: Theme }) => ({
        fontSizeFromScreens: '74px',
        fontSizeFromScreensMin: { min: '470px' },
        fontSizeFromScreensMax: { max: '470px' },
        fontSizeFromScreensScreen: theme('screens.xl'),
      }),
      fontSizeToScreens: ({ theme }: { theme: Theme }) => ({
        fontSizeToScreens: '470px',
        fontSizeToScreensMin: { min: '470px' },
        fontSizeToScreensMax: { max: '470px' },
        fontSizeToScreensScreen: theme('screens.xl'),
      }),
    },
  };

  const input = css`
    @tailwind utilities;
  `;

  await run(input, config).then((result) => {
    // @ts-expect-error ts(2339)
    expect(result.css).toMatchFormattedCss(css`
      .text-from-sm\@screens {
        --fluid-font-size--from-font-size-px: 14;
        --fluid-font-size--from-screen-px: 47;
      }
      .text-from-sm\@screensMin {
        --fluid-font-size--from-font-size-px: 14;
        --fluid-font-size--from-screen-px: 470;
      }
      .text-from-sm\@screensMax {
        --fluid-font-size--from-font-size-px: 14;
        --fluid-font-size--from-screen-px: 470;
      }

      .text-from-sm\@fontSizeScreens {
        --fluid-font-size--from-font-size-px: 14;
        --fluid-font-size--from-screen-px: 47;
      }
      .text-from-sm\@fontSizeScreensMin {
        --fluid-font-size--from-font-size-px: 14;
        --fluid-font-size--from-screen-px: 470;
      }
      .text-from-sm\@fontSizeScreensMax {
        --fluid-font-size--from-font-size-px: 14;
        --fluid-font-size--from-screen-px: 470;
      }
      .text-from-sm\@fontSizeScreensScreen {
        --fluid-font-size--from-font-size-px: 14;
        --fluid-font-size--from-screen-px: 1280;
      }

      .text-from-sm\@fontSizeFromScreens {
        --fluid-font-size--from-font-size-px: 14;
        --fluid-font-size--from-screen-px: 74;
      }
      .text-from-sm\@fontSizeFromScreensMin {
        --fluid-font-size--from-font-size-px: 14;
        --fluid-font-size--from-screen-px: 470;
      }
      .text-from-sm\@fontSizeFromScreensMax {
        --fluid-font-size--from-font-size-px: 14;
        --fluid-font-size--from-screen-px: 470;
      }
      .text-from-sm\@fontSizeFromScreensScreen {
        --fluid-font-size--from-font-size-px: 14;
        --fluid-font-size--from-screen-px: 1280;
      }

      .text-to-sm\@screens {
        --fluid-font-size--to-font-size-px: 14;
        --fluid-font-size--to-screen-px: 47;
      }
      .text-to-sm\@screensMin {
        --fluid-font-size--to-font-size-px: 14;
        --fluid-font-size--to-screen-px: 470;
      }
      .text-to-sm\@screensMax {
        --fluid-font-size--to-font-size-px: 14;
        --fluid-font-size--to-screen-px: 470;
      }

      .text-to-sm\@fontSizeScreens {
        --fluid-font-size--to-font-size-px: 14;
        --fluid-font-size--to-screen-px: 47;
      }
      .text-to-sm\@fontSizeScreensMin {
        --fluid-font-size--to-font-size-px: 14;
        --fluid-font-size--to-screen-px: 470;
      }
      .text-to-sm\@fontSizeScreensMax {
        --fluid-font-size--to-font-size-px: 14;
        --fluid-font-size--to-screen-px: 470;
      }

      .text-to-sm\@fontSizeToScreens {
        --fluid-font-size--to-font-size-px: 14;
        --fluid-font-size--to-screen-px: 470;
      }
      .text-to-sm\@fontSizeToScreensMin {
        --fluid-font-size--to-font-size-px: 14;
        --fluid-font-size--to-screen-px: 470;
      }
      .text-to-sm\@fontSizeToScreensMax {
        --fluid-font-size--to-font-size-px: 14;
        --fluid-font-size--to-screen-px: 470;
      }
      .text-to-sm\@fontSizeToScreensScreen {
        --fluid-font-size--to-font-size-px: 14;
        --fluid-font-size--to-screen-px: 1280;
      }
    `);
  });
});

it('should ignore unsupported custom screens', async () => {
  const config = {
    content: [
      {
        raw: html`
          <div>
            <div class="text-from-sm@screens text-to-sm@screens"></div>
            <div
              class="text-from-sm@fontSizeScreens text-to-sm@fontSizeScreens"
            ></div>
            <div
              class="text-from-sm@fontSizeFromScreens text-to-sm@fontSizeFromScreens"
            ></div>
            <div
              class="text-from-md@fontSizeToScreens text-to-md@fontSizeToScreens"
            ></div>
            <div
              class="text-from-md@fontSizeToScreensMin text-to-md@fontSizeToScreensMin"
            ></div>
            <div
              class="text-from-md@fontSizeToScreensMax text-to-md@fontSizeToScreensMax"
            ></div>
            <div
              class="text-from-md@fontSizeToScreensMinMax text-to-md@fontSizeToScreensMinMax"
            ></div>
            <div
              class="text-from-md@fontSizeToScreensRaw text-to-md@fontSizeToScreensRaw"
            ></div>
          </div>
        `,
      },
    ],
    theme: {
      extend: {
        screens: {
          screensMinMax: { min: '470px', max: '470px' },
          screensRaw: { raw: 'print' },
        },
      },
      fontSizeScreens: {
        fontSizeScreensMinMax: { min: '470px', max: '470px' },
        fontSizeScreensRaw: { raw: 'print' },
      },
      fontSizeFromScreens: {
        fontSizeFromScreensMinMax: { min: '470px', max: '470px' },
        fontSizeFromScreensRaw: { raw: 'print' },
      },
      fontSizeToScreens: {
        fontSizeToScreensMinMax: { min: '470px', max: '470px' },
        fontSizeToScreensRaw: { raw: 'print' },
      },
    },
  };

  const input = css`
    @tailwind utilities;
  `;

  await run(input, config).then((result) => {
    // @ts-expect-error ts(2339)
    expect(result.css).toMatchFormattedCss(css``);
  });
});

it('should add warning comments for unsupported configured and arbitrary values', async () => {
  const config = {
    content: [
      {
        raw: html`
          <div class="text-from-unsupported1@unsupported1"></div>
          <div class="text-from-unsupported2@unsupported1"></div>
          <div class="text-from-unsupported1@unsupported2"></div>
          <div class="text-from-unsupported2@unsupported2"></div>

          <div class="text-from-[min(1px,_2px)@1ch]"></div>
          <div class="text-from-[2ch@1ch]"></div>
          <div class="text-from-[min(1px,_2px)@helloworld]"></div>
          <div class="text-from-[2ch@helloworld]"></div>
        `,
      },
    ],
    theme: {
      screens: {
        unsupported1: '1ch',
        unsupported2: 'helloworld',
      },
      fontSize: {
        unsupported1: 'min(1px, 2px)',
        unsupported2: '2ch',
      },
    },
  };

  const input = css`
    @tailwind utilities;
  `;

  await run(input, config).then((result) => {
    // @ts-expect-error ts(2339)
    expect(result.css).toMatchFormattedCss(css`
      .text-from-unsupported1\@unsupported1 {
        --fluid-font-size--from-font-size-px: min(1px, 2px)
          /* Warning: 'text-from font-size' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem'). */;
        --fluid-font-size--from-font-size--warning: "Warning: 'text-from font-size' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem').";
        --fluid-font-size--from-screen-px: 1ch
          /* Warning: 'text-from screen' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem'). */;
        --fluid-font-size--from-screen--warning: "Warning: 'text-from screen' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem').";
      }
      .text-from-unsupported1\@unsupported2 {
        --fluid-font-size--from-font-size-px: min(1px, 2px)
          /* Warning: 'text-from font-size' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem'). */;
        --fluid-font-size--from-font-size--warning: "Warning: 'text-from font-size' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem').";
        --fluid-font-size--from-screen-px: helloworld
          /* Warning: 'text-from screen' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem'). */;
        --fluid-font-size--from-screen--warning: "Warning: 'text-from screen' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem').";
      }
      .text-from-unsupported2\@unsupported1 {
        --fluid-font-size--from-font-size-px: 2ch
          /* Warning: 'text-from font-size' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem'). */;
        --fluid-font-size--from-font-size--warning: "Warning: 'text-from font-size' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem').";
        --fluid-font-size--from-screen-px: 1ch
          /* Warning: 'text-from screen' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem'). */;
        --fluid-font-size--from-screen--warning: "Warning: 'text-from screen' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem').";
      }
      .text-from-unsupported2\@unsupported2 {
        --fluid-font-size--from-font-size-px: 2ch
          /* Warning: 'text-from font-size' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem'). */;
        --fluid-font-size--from-font-size--warning: "Warning: 'text-from font-size' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem').";
        --fluid-font-size--from-screen-px: helloworld
          /* Warning: 'text-from screen' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem'). */;
        --fluid-font-size--from-screen--warning: "Warning: 'text-from screen' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem').";
      }
      .text-from-\[2ch\@1ch\] {
        --fluid-font-size--from-font-size-px: 2ch
          /* Warning: 'text-from font-size' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem'). */;
        --fluid-font-size--from-font-size--warning: "Warning: 'text-from font-size' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem').";
        --fluid-font-size--from-screen-px: 1ch
          /* Warning: 'text-from screen' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem'). */;
        --fluid-font-size--from-screen--warning: "Warning: 'text-from screen' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem').";
      }
      .text-from-\[2ch\@helloworld\] {
        --fluid-font-size--from-font-size-px: 2ch
          /* Warning: 'text-from font-size' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem'). */;
        --fluid-font-size--from-font-size--warning: "Warning: 'text-from font-size' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem').";
        --fluid-font-size--from-screen-px: helloworld
          /* Warning: 'text-from screen' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem'). */;
        --fluid-font-size--from-screen--warning: "Warning: 'text-from screen' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem').";
      }
      .text-from-\[min\(1px\2c _2px\)\@1ch\] {
        --fluid-font-size--from-font-size-px: min(1px, 2px)
          /* Warning: 'text-from font-size' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem'). */;
        --fluid-font-size--from-font-size--warning: "Warning: 'text-from font-size' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem').";
        --fluid-font-size--from-screen-px: 1ch
          /* Warning: 'text-from screen' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem'). */;
        --fluid-font-size--from-screen--warning: "Warning: 'text-from screen' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem').";
      }
      .text-from-\[min\(1px\2c _2px\)\@helloworld\] {
        --fluid-font-size--from-font-size-px: min(1px, 2px)
          /* Warning: 'text-from font-size' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem'). */;
        --fluid-font-size--from-font-size--warning: "Warning: 'text-from font-size' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem').";
        --fluid-font-size--from-screen-px: helloworld
          /* Warning: 'text-from screen' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem'). */;
        --fluid-font-size--from-screen--warning: "Warning: 'text-from screen' value should be a pixel length (e.g. '16px') or a rem length (e.g. '1rem').";
      }
    `);
  });
});

it('should play nice with official container query plugin', async () => {
  /**
   * https://github.com/tailwindlabs/tailwindcss-container-queries also uses '@'
   */
  const config = {
    content: [
      {
        raw: html`<div class="@container">
          <div
            class="text-fluid @lg:underline text-from-px@lg text-from-[1rem@sm]"
          ></div>
        </div>`,
      },
    ],
    plugins: [containerQueriesPlugin, fluidFontSizePlugin],
  };

  const input = css`
    @tailwind utilities;
  `;

  await run(input, config).then((result) => {
    // @ts-expect-error ts(2339)
    expect(result.css).toMatchFormattedCss(css`
      .\@container {
        container-type: inline-size;
      }
      .text-fluid {
        --fluid-font-size--slope: calc(
          calc(
              var(--fluid-font-size--from-font-size-px) -
                var(--fluid-font-size--to-font-size-px)
            ) /
            calc(
              var(--fluid-font-size--from-screen-px) -
                var(--fluid-font-size--to-screen-px)
            )
        );
        --fluid-font-size--intercept-px: calc(
          var(--fluid-font-size--from-font-size-px) -
            var(--fluid-font-size--slope) *
            var(--fluid-font-size--from-screen-px)
        );
        --fluid-font-size--val: calc(
          calc(
              100 * var(--fluid-font-size--relative-unit) *
                var(--fluid-font-size--slope)
            ) + calc(1rem * var(--fluid-font-size--intercept-px) / 16)
        );
        --fluid-font-size--min: calc(
          1rem *
            min(
              var(--fluid-font-size--to-font-size-px),
              var(--fluid-font-size--from-font-size-px)
            ) / 16
        );
        --fluid-font-size--max: calc(
          1rem *
            max(
              var(--fluid-font-size--to-font-size-px),
              var(--fluid-font-size--from-font-size-px)
            ) / 16
        );
        font-size: clamp(
          var(--fluid-font-size--min),
          var(--fluid-font-size--val),
          var(--fluid-font-size--max)
        );
      }
      .text-from-\[1rem\@sm\] {
        --fluid-font-size--from-font-size-px: 16;
        --fluid-font-size--from-screen-px: 640;
      }
      @container (min-width: 32rem) {
        .\@lg\:underline {
          text-decoration-line: underline;
        }
      }
    `);
  });
});

it('should support configuring plugin via options', async () => {
  const config = {
    content: [
      {
        raw: html`<div
          class="p-from-18px-to-36px text-fluid text-from-xs text-to-9xl"
        ></div>`,
      },
    ],
    plugins: [
      fluidFontSizePlugin({
        fromScreen: '470px',
        fromFontSize: '18px',
        pretty: false,
        relativeUnit: 'dvw',
        toScreen: '740px',
        toFontSize: '36px',
      }),
    ],
  };

  const input = css`
    @tailwind base;
    @tailwind utilities;
  `;

  await run(input, config).then((result) => {
    // @ts-expect-error ts(2339)
    expect(result.css).toMatchFormattedCss(css`
      ${
        /**
         * start from Tailwind's @base layer
         */
        ''
      }
      *,
      ::before,
      ::after {
        --tw-border-spacing-x: 0;
        --tw-border-spacing-y: 0;
        --tw-translate-x: 0;
        --tw-translate-y: 0;
        --tw-rotate: 0;
        --tw-skew-x: 0;
        --tw-skew-y: 0;
        --tw-scale-x: 1;
        --tw-scale-y: 1;
        --tw-pan-x: ;
        --tw-pan-y: ;
        --tw-pinch-zoom: ;
        --tw-scroll-snap-strictness: proximity;
        --tw-gradient-from-position: ;
        --tw-gradient-via-position: ;
        --tw-gradient-to-position: ;
        --tw-ordinal: ;
        --tw-slashed-zero: ;
        --tw-numeric-figure: ;
        --tw-numeric-spacing: ;
        --tw-numeric-fraction: ;
        --tw-ring-inset: ;
        --tw-ring-offset-width: 0px;
        --tw-ring-offset-color: #fff;
        --tw-ring-color: rgb(59 130 246 / 0.5);
        --tw-ring-offset-shadow: 0 0 #0000;
        --tw-ring-shadow: 0 0 #0000;
        --tw-shadow: 0 0 #0000;
        --tw-shadow-colored: 0 0 #0000;
        --tw-blur: ;
        --tw-brightness: ;
        --tw-contrast: ;
        --tw-grayscale: ;
        --tw-hue-rotate: ;
        --tw-invert: ;
        --tw-saturate: ;
        --tw-sepia: ;
        --tw-drop-shadow: ;
        --tw-backdrop-blur: ;
        --tw-backdrop-brightness: ;
        --tw-backdrop-contrast: ;
        --tw-backdrop-grayscale: ;
        --tw-backdrop-hue-rotate: ;
        --tw-backdrop-invert: ;
        --tw-backdrop-opacity: ;
        --tw-backdrop-saturate: ;
        --tw-backdrop-sepia: ;
        --tw-contain-size: ;
        --tw-contain-layout: ;
        --tw-contain-paint: ;
        --tw-contain-style: ;
      }
      ::backdrop {
        --tw-border-spacing-x: 0;
        --tw-border-spacing-y: 0;
        --tw-translate-x: 0;
        --tw-translate-y: 0;
        --tw-rotate: 0;
        --tw-skew-x: 0;
        --tw-skew-y: 0;
        --tw-scale-x: 1;
        --tw-scale-y: 1;
        --tw-pan-x: ;
        --tw-pan-y: ;
        --tw-pinch-zoom: ;
        --tw-scroll-snap-strictness: proximity;
        --tw-gradient-from-position: ;
        --tw-gradient-via-position: ;
        --tw-gradient-to-position: ;
        --tw-ordinal: ;
        --tw-slashed-zero: ;
        --tw-numeric-figure: ;
        --tw-numeric-spacing: ;
        --tw-numeric-fraction: ;
        --tw-ring-inset: ;
        --tw-ring-offset-width: 0px;
        --tw-ring-offset-color: #fff;
        --tw-ring-color: rgb(59 130 246 / 0.5);
        --tw-ring-offset-shadow: 0 0 #0000;
        --tw-ring-shadow: 0 0 #0000;
        --tw-shadow: 0 0 #0000;
        --tw-shadow-colored: 0 0 #0000;
        --tw-blur: ;
        --tw-brightness: ;
        --tw-contrast: ;
        --tw-grayscale: ;
        --tw-hue-rotate: ;
        --tw-invert: ;
        --tw-saturate: ;
        --tw-sepia: ;
        --tw-drop-shadow: ;
        --tw-backdrop-blur: ;
        --tw-backdrop-brightness: ;
        --tw-backdrop-contrast: ;
        --tw-backdrop-grayscale: ;
        --tw-backdrop-hue-rotate: ;
        --tw-backdrop-invert: ;
        --tw-backdrop-opacity: ;
        --tw-backdrop-saturate: ;
        --tw-backdrop-sepia: ;
        --tw-contain-size: ;
        --tw-contain-layout: ;
        --tw-contain-paint: ;
        --tw-contain-style: ;
      }
      ${
        /**
         * end from Tailwind's @base layer
         */
        ''
      }
      :root {
        --fluid-font-size--from-screen-px: 470;
        --fluid-font-size--from-font-size-px: 18;
        --fluid-font-size--relative-unit: 1dvw;
        --fluid-font-size--to-screen-px: 740;
        --fluid-font-size--to-font-size-px: 36;
      }
      .text-fluid {
        font-size: clamp(
          calc(
            1rem *
              min(
                var(--fluid-font-size--to-font-size-px),
                var(--fluid-font-size--from-font-size-px)
              ) / 16
          ),
          calc(
            calc(
                100 * var(--fluid-font-size--relative-unit) *
                  calc(
                    calc(
                        var(--fluid-font-size--from-font-size-px) -
                          var(--fluid-font-size--to-font-size-px)
                      ) /
                      calc(
                        var(--fluid-font-size--from-screen-px) -
                          var(--fluid-font-size--to-screen-px)
                      )
                  )
              ) +
              calc(
                1rem *
                  calc(
                    var(--fluid-font-size--from-font-size-px) -
                      calc(
                        calc(
                            var(--fluid-font-size--from-font-size-px) -
                              var(--fluid-font-size--to-font-size-px)
                          ) /
                          calc(
                            var(--fluid-font-size--from-screen-px) -
                              var(--fluid-font-size--to-screen-px)
                          )
                      ) * var(--fluid-font-size--from-screen-px)
                  ) / 16
              )
          ),
          calc(
            1rem *
              max(
                var(--fluid-font-size--to-font-size-px),
                var(--fluid-font-size--from-font-size-px)
              ) / 16
          )
        );
      }

      .text-from-xs {
        --fluid-font-size--from-font-size-px: 12;
      }

      .text-to-9xl {
        --fluid-font-size--to-font-size-px: 128;
      }
    `);
  });
});
