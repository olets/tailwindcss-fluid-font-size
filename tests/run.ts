import fluidSizingPlugin from '../src';
import postcss from 'postcss';
import tailwind, { type Config } from 'tailwindcss';
import path from 'path';

export const css = String.raw;
export const html = String.raw;
export const javascript = String.raw;

export function run(input: string, config: Config, plugin = tailwind) {
  const { currentTestName } = expect.getState();

  config.corePlugins = {
    preflight: false,
  };

  config.plugins = config?.plugins ?? [fluidSizingPlugin];

  return postcss(plugin(config)).process(input, {
    from: `${path.resolve(__filename)}?test=${currentTestName}`,
  });
}
